const path = require('path');
const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
    cache: true,
    entry: './src/index.jsx',
    output: {
        path: path.join(__dirname, '/public'),
        publicPath: '/public',
        filename: 'bundle.js'
    },
    module: {
        loaders: [{
            test: /\.jsx?$/,
            include: path.join(__dirname, 'src'),
            exclude: /node_modules/,
            loader: "babel-loader",
            query: {
                presets: ['es2015', 'react', 'stage-0', 'stage-3']
            }
        },
        {
            test: /\.css$/,
            exclude: /node_modules/,
            use: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: [
                    {
                        loader: 'css-loader',
                        query: {
                            modules: true,
                            localIdentName: '[local]',
                        }
                    },
                    'postcss-loader'
                ]
            }),
        },
        {
            test: /\.scss$/,
            exclude: /node_modules/,
            use: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: [
                    {
                        loader: 'css-loader',
                        query: {
                            modules: true,
                            sourceMap: true,
                            importLoaders: 2,
                            localIdentName: '[local]',
                        }
                    },
                    'sass-loader'
                ]
            }),
        },
        {
            test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
            loader: "file-loader"
        },
        {
            test: /\.(jpe?g|png|gif|svg)$/i,
            loaders: ['file-loader?name=/css/lib/[name].[ext]', {
              loader: 'image-webpack-loader',
              query: {
                mozjpeg: {
                  progressive: true
                },
                gifsicle: {
                  interlaced: false
                },
                optipng: {
                  optimizationLevel: 4
                },
                pngquant: {
                  quality: '75-90',
                  speed: 3
                }
              }
            }],
            exclude: /node_modules/
        }
      ]
    },
    plugins: [
      new webpack.LoaderOptionsPlugin({
        minimize: true,
        debug: false
      }),
      new webpack.optimize.UglifyJsPlugin({
        beautify: false,
        mangle: {
          screw_ie8: true,
          keep_fnames: true
        },
        comments: false
      }),
      new webpack.optimize.AggressiveMergingPlugin(),
      new ExtractTextPlugin( "styles/style.css" ),
      new CompressionPlugin({
        asset: "[path].gz[query]",
        algorithm: "gzip",
        test: /\.js$|\.css$/,
        threshold: 10240,
        minRatio: 0.8
      }),
      // new HtmlWebpackPlugin(),
      new CopyWebpackPlugin([{
        from: '/src/assets/img',
        to: 'img'
      }])

    ],
    resolve: {
      alias: {
        "eventEmitter/EventEmitter": "wolfy87-eventemitter"
      },
      extensions: ['.js', '.jsx']
},
};

process.noDeprecation = true;

