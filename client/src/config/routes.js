import React from 'react';
import {Route, IndexRoute} from 'react-router';


// Auth
import DefaultLayout from '../layouts/Default'; // The layout that wraps the signin pages
import Signin from '../components/auth/signin';
import Signout from '../components/auth/signout';
import Signup from '../components/auth/signup';
import AdminActivation from '../components/auth/admin_activation';
import RequireAuth from '../components/auth/require_auth';
import RequireAdmin from '../components/auth/require_admin';

// Agent
import AgentLayout from '../layouts/Agent'; // The layout that wraps the agent pages
import DashboardPage from '../pages/agent/Dashboard';
import AgentQueuePage from '../pages/agent/AgentQueue';
import AgentChatPage from '../pages/agent/AgentChat';
import AgentLogsPage from '../pages/agent/AgentLogs';

// Admin - TODO
import AdminArea from '../pages/admin/Admin';

// Customer
import CustomerLayout from '../layouts/Customer'; // The layout that wraps the customer pages
import CustomerInitiationPage from '../pages/customer/CustomerInitiation';
import CustomerQueuePage from '../pages/customer/CustomerQueue';
import CustomerChatPage from '../pages/customer/CustomerChat';
import CustomerReviewPage from '../pages/customer/CustomerReview';
import CustomerFinishPage from '../pages/customer/CustomerFinish';

export function routes() {

  return (
    <Route>
      <Route path="/" component={DefaultLayout}>
        <IndexRoute component={Signin} />
        <Route path="signin" component={Signin} />
        <Route path="signout" component={Signout} />
        <Route path="signup" component={Signup} />
      </Route>
      <Route path="/agent/" component={AgentLayout}>
        <IndexRoute component={RequireAuth(DashboardPage)} />
        <Route path="dashboard" component={RequireAuth(DashboardPage)} />
        <Route path="queue" component={RequireAuth(AgentQueuePage)} />
        <Route path="chat" component={RequireAuth(AgentChatPage)} />
        <Route path="logs" component={RequireAuth(AgentLogsPage)} />
      </Route>
      <Route path="/customer" component={CustomerLayout}>
        <IndexRoute component={CustomerInitiationPage} />
        <Route path="queue" component={CustomerQueuePage} />
        <Route path="chat" component={CustomerChatPage} />
        <Route path="review" component={CustomerReviewPage} />
        <Route path="finish" component={CustomerFinishPage} />
      </Route>
    </Route>
  );
}

export default routes;


