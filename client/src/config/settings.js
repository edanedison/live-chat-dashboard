const settings = {
	title: 'Live Chat',
	api: {
		socketUrl: 'http://localhost:3000'
	},
	chat: {
		typingTimeout: 1000,
		activityTimeout: 6000000
	}
};

export default settings;