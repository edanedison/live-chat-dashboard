import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { reducer as form } from 'redux-form';
import auth from './authReducer';
import session from './sessionReducer';
import { reducers as swalReducers } from 'react-redux-sweetalert2';

const rootReducer = combineReducers({
  swal: swalReducers,
  form,
  auth,
  session
});

export default rootReducer;
