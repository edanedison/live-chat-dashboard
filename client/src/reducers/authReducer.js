
import * as types from '../actions/types';
import initialState from './initialState';


export default function authReducer(state = initialState.auth, action) {
  switch (action.type) {
    case types.AUTH_USER:
      return Object.assign({}, state, {
        error: '',
        authenticated: true
      });
    case types.UNAUTH_USER:
      return Object.assign({}, state, {
        authenticated: false,
        admin_privileges: false
      });
    case types.UPDATE_PROFILE:
      return Object.assign({}, state, {
        profile: {
          firstName: action.value.profile.firstName,
          lastName: action.value.profile.lastName,
          photo: action.value.profile.photo,
          email: action.value.email
        }
      });
    case types.AUTH_ERROR:
      return Object.assign({}, state, {
        error: action.value
      });
    case types.FETCH_MESSAGE:
      return Object.assign({}, state, {
        message: action.value
      });
    case types.FETCH_ADMIN_MESSAGE:
      return Object.assign({}, state, {
        message: action.value
      });
    case types.SET_ADMIN_PRIVILEGES:
      return Object.assign({}, state, {
        admin_privileges: true
      });
    default:
      return state;
  }
}