import * as types from '../actions/types';
import initialState from './initialState';

export default function sessionReducer(state = initialState.session, action) {

  switch (action.type) {
    case types.INITIATE_SESSION_SUCCESS:
      return Object.assign({}, state, {
        id: action.value.sessionId,
        agentName: action.value.agentName,
        customerName: action.value.customerName,
        active: true,
        initiatedAt: Date.now()
      });
    case types.SUSPEND_SESSION_SUCCESS:
      return Object.assign({}, state, {
        active: false
      });
    case types.TERMINATE_SESSION_SUCCESS:
      return Object.assign({}, state, {
        id: action.value,
        active: false,
        terminatedAt: Date.now()
      });
    case types.UPDATE_AGENT_ROLE:
      return Object.assign({}, state, {
        agent: action.value
      });
    case types.UPDATE_AGENT_AVAILABILITY:
      return Object.assign({}, state, {
        agentAvailable: action.value
      });
    case types.UPDATE_AGENT_COUNT:
      return Object.assign({}, state, {
        agentCount: action.value
      });
    case types.UPDATE_SESSION_PHOTO_SUCCESS:
      return Object.assign({}, state, {
        agentPhoto: action.value
      });
    case types.UPDATE_CUSTOMER_ID_SUCCESS:
      return Object.assign({}, state, {
        customerId: action.value,
        queueing: true
      });
    case types.UPDATE_AGENT_QUEUE:
      return Object.assign({}, state, {
        logs: action.value
      });
    case types.GET_LOGS_SUCCESS:
      return Object.assign({}, state, {
        logs: action.value
      });
    case types.GET_TRANSCIPT_SUCCESS:
      return Object.assign({}, state, {
        transcript: action.value
      });
    case types.UPDATE_TRANSCRIPT_SUCCESS:
      return Object.assign({}, state, {
        transcript: action.value
      });
    case types.UPDATE_QUEUE_POSITION:
      return Object.assign({}, state, {
        position: action.value.position
      });
    case types.UPDATE_STATS_SUCCESS:
      return Object.assign({}, state, {
        stats: {
          waiting: action.value.waiting.data.length > 0 ? action.value.waiting.data.length : 0,
          inProgress: action.value.inProgress.data.length > 0 ? action.value.inProgress.data.length : 0,
          completed: action.value.completed.data.length > 0 ? action.value.completed.data.length : 0,
          dropped: action.value.dropped.data.length > 0 ? action.value.dropped.data.length : 0
        }
      });
    default:
      return state;
  }
}


