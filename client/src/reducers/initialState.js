export default {
  auth: {
    authenticated: false,
    admin_privileges: false,
    profile: {
      firstName: '',
      lastName: '',
      email: '',
      photo: '/img/default-avatar.png'
    }
  },
  session: {
    id: null,
    active: false,
    agentCount: 0,
    initiatedAt: null,
    terminatedAt: null,
    agent: false,
    agentName: null,
    agentPhoto: '/img/default-avatar.png',
    agentAvailable: false,
    customerId: null,
    queueing: false,
    customerName: null,
    position: 1,
    transcript: [],
    logs: [],
    stats: {
      waiting: 0,
      inProgress: 0,
      completed: 0,
      dropped: 0
    }
  }
};

// import image correctly