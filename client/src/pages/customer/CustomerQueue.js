import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import { initiateSession, updateCustomerId, updateQueuePosition, updateAgentCount } from '../../actions/sessionActions';

import Api from '../../api/api';

export class CustomerQueue extends Component {

    constructor(props) {
        super(props);
        this.api = new Api();
    }

  componentDidMount = () => {

      if (!sessionStorage.getItem('addToQueue')) {
        this.props.actions.updateQueuePosition(sessionStorage.getItem('sessionId'));
      }

      // Emit a socket action to check the the current session is valid

      this.api.socket.emit('CHECK_CURRENT_SESSION', {
          sessionId: sessionStorage.getItem('sessionId'),
          user: 'Customer'
      });

      // Update the customer queue position

      this.api.socket.on('UPDATE_QUEUE_POSITIONS', () => {
        this.props.actions.updateQueuePosition(sessionStorage.getItem('sessionId'));
      });

      // Update the number of available agents

      this.api.socket.on('UPDATE_AVAILABLE_AGENTS', () => {
        this.props.actions.updateAgentCount();
      });

      // Add the customer to the queue provided they carry a session token

      this.api.socket.on('CUSTOMER_CONNECTED', (data) => {
        if (sessionStorage.getItem('addToQueue')) {
          sessionStorage.setItem('sessionId', data.sessionId);
          this.props.actions.updateQueuePosition(data.sessionId);
          this.props.actions.updateCustomerId(data.sessionId);
        }
      });

      this.api.socket.on('OPEN_CHAT_ROOM', (data) => {

         // Making sure that the correct session details are shared to the selected customer and no-one else

        if (sessionStorage.getItem('sessionId') === data.sessionId) {
          this.props.actions.initiateSession(data.sessionId, data.customerName, data.agentName);
          this.props.router.push('/customer/chat');

        } else {
          console.log("You are not the chosen one!");
          this.props.actions.updateQueuePosition(sessionStorage.getItem('sessionId'));
        }

      });


    }

  componentDidUpdate = () => {

      // If there are suddenly no agents, then return the customer to the start page

      if (this.props.session.agentCount === 0) {
        this.props.router.push('/customer');
      }

      // TODO: Add to redux store not session storage for these items

      if (!this.props.session.queueing) {
          this.props.router.push('/customer');
      }

      if (sessionStorage.getItem('addToQueue')) {

       this.api.socket.emit('CUSTOMER_WAITING', {
            name: sessionStorage.getItem('name'),
            email: sessionStorage.getItem('email'),
            orderNo: sessionStorage.getItem('orderNo')
        });

       sessionStorage.removeItem('addToQueue');

      }
    }

  componentWillUnmount = () => {
    this.api.socket.disconnect(true);
  }

  handleChange = (e) => {
      this.keyCount++;
      this.setState({
          name: e.target.value
      });
  }

  handleKeyPress = (e) => {
      if (e.key === 'Enter') {
          this.connectToAgent();
      }
  }

  render = () => {

    return (
      <div className="container">
        <div className="content">
          <div className="card">
            <div className="card-header" data-background-color="purple">
            <b>Please wait for an available agent...</b>
            </div>
            <div className="card-content">
            <h5><b>You are position {this.props.session.position} in the queue</b></h5>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

CustomerQueue.propTypes = {
  actions: PropTypes.object,
  route: PropTypes.object
};

function mapStateToProps(state) {
  return {
    session: state.session,
    stats: state.session.stats
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      updateCustomerId,
      updateQueuePosition,
      updateAgentCount,
      initiateSession
    }, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CustomerQueue);