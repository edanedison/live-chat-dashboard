import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Api from '../../api/api';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { push } from 'react-router-redux';
import { updateAgentCount } from '../../actions/sessionActions';
import { bindActionCreators } from 'redux';

let  converter = require('number-to-words');

export class CustomerInitiation extends Component {

    constructor(props) {
        super(props);
        this.api = new Api();
        this.state = {
            user: {
              email: '',
              name: '',
              orderNo: ''
            },
            addToQueue: false,
            agentCount: 0,
            time: Date.now()
        };
    }

    componentWillMount = () => {

      this.props.actions.updateAgentCount();
      sessionStorage.removeItem('id');

    }

    componentDidMount = () => {

      // Emit a socket action to check the the current session is valid

      this.api.socket.emit('CHECK_CURRENT_SESSION', {
          sessionId: sessionStorage.getItem('sessionId'),
          user: 'Agent'
      });

      this.api.socket.on('UPDATE_AVAILABLE_AGENTS', () => {
        this.props.actions.updateAgentCount();
      });

    }

    componentDidUpdate = () => {

      sessionStorage.setItem('name', this.state.user.name);
      sessionStorage.setItem('email', this.state.user.email);
      sessionStorage.setItem('orderNo', this.state.user.orderNo);

      if (this.state.addToQueue) {
         sessionStorage.setItem('addToQueue', true); // TODO: Move to redux store
         this.props.router.push('/customer/queue');
      }
    }

    handleChange = (e) => {

        let field = e.target.name,
            user = this.state.user;

        user[field] = e.target.value;

        this.setState({
          user
        });
    }

    handleKeyPress = (e) => {

        if (e.key === 'Enter') {
            this.validate();
        }
    }

    validate = () => {

      if (this.state.user.name.length > 0 && this.state.user.email.length > 0) {
        this.setState({
            addToQueue: true
        });
      } else {
        alert("Please enter a name and email address");
      }

    }

    render = () => {

    const count = converter.toWords(this.props.session.agentCount);

    const form = this.props.session.agentCount > 0 ? (

    <div className="content">
      <div className="card">
        <div className="card-header" data-background-color="purple">
          <b>Need help? Our live chat agents are available.</b>
      </div>
     <div className="card-content">
          <h5><b>Please give us a few details to get started</b></h5>
          <div className="form-group label-floating is-empty">
              <label className="control-label">Your name</label>
              <input name="name" value={this.state.name} onChange={this.handleChange} onKeyPress={this.handleKeyPress} type="name" className="form-control" />
              <span className="material-input" />
          </div>
          <div className="form-group label-floating is-empty">
              <label className="control-label">Your email</label>
              <input name="email" value={this.state.email} onChange={this.handleChange} onKeyPress={this.handleKeyPress} type="email" className="form-control" />
              <span className="material-input" />
          </div>
          {/*<div className="form-group label-floating is-empty">
              <label className="control-label">Your order number (if applicable)</label>
              <input name="orderNo" value={this.state.orderNo} onChange={this.handleChange} onKeyPress={this.handleKeyPress} type="orderNo" className="form-control" />
              <span className="material-input" />
          </div>*/}
          <button type="submit" className="btn btn-lg btn-success" onClick={this.validate}><b>Connect</b></button>
<p>{this.props.session.agentCount} of our chat agents {this.props.session.agentCount === 1 ? 'is' : 'are'} online</p>
        </div>
      </div>
    </div>
    ) : (

    <div className="content">
      <div className="card">
        <div className="card-header" data-background-color="purple">
        <b>Sorry! Our live chat service is not available at this time</b>
        </div>
        <div className="card-content">
        <h5><b>Please email us at <a href="mailto:support@claires.com">support@claires.com</a> or give us a call on 0121 111111</b></h5>
        </div>
      </div>
    </div>

    );

    return (
      <div className="container">



          {form}

      </div>
    );
  }
}

CustomerInitiation.propTypes = {
  actions: PropTypes.object,
  dispatch: PropTypes.object,
  route: PropTypes.object
};

function mapStateToProps(state, ownProps) {
  return {
    session: state.session
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      updateAgentCount
    }, dispatch)
  };
}



export default connect(mapStateToProps, mapDispatchToProps)(CustomerInitiation);