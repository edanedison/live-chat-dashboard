import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import moment from 'moment';

export class CustomerReview extends Component {

  constructor(props) {
      super(props);
  }

  componentWillMount = () => {

      if (!this.props.session.id) {
          this.props.router.push('/customer');
      }

      sessionStorage.removeItem('sessionId');

  }

  sendTranscript = () => {
    // TODO: Email the chat transcript
  }

  redirectUser = () => {
     this.props.router.push('/customer');
  }

  render = () => {

    // Present the chat transcript to the user

    let transcript = this.props.session.transcript.map((item, i) => {

      const createdBy = item.agent ? this.props.session.agentName : 'You';

      return (
            <li key={i} style={{padding: '1rem 2rem', borderBottom: '1px solid #fff', background: item.agent ? '#efe8ff' : 'white'}}>
              <p><b><small className="text-muted">{createdBy} - {moment(item.createdAt).format('DD-MMM-YYYY h:mm:ss a')}</small></b><br/>
                {item.message}
              </p>
            </li>
       );
    });

    return (

          <div className="chat-wrapper">
            <div className="chat-header">
            <h4>Your live chat session has ended</h4>
              <div role="group" aria-label="...">
                <button type="submit" className="btn btn-fill" onClick={this.sendTranscript}><i className="material-icons">mail_outline</i> Email transcript</button>
                <button type="submit" className="btn btn-fill btn-success" onClick={this.redirectUser}> <i className="material-icons">check</i> Finished</button>
              </div>
            </div>
            <div className="chat-summary">
              <ul className="media-list">
                {transcript}
              </ul>
            </div>
          </div>
    );
  }
}

CustomerReview.propTypes = {
  agentName: PropTypes.string,
  actions: PropTypes.object,
  dispatch: PropTypes.object,
  route: PropTypes.object
};


function mapStateToProps(state, ownProps) {
  return {
    session: state.session,
    chat: state.chat
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
    }, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CustomerReview);