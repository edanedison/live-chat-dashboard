import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { bindActionCreators } from 'redux';

import { updateAgentRole, updateChatTranscript, initiateSession, terminateSession, updateAgentPhoto } from '../../actions/sessionActions';

import { MessageBox, MessageList } from '../../components/Messages';

import Settings from '../../config/settings';
import Api from '../../api/api';

export class CustomerChat extends Component {

    constructor(props) {
        super(props);
        this.api = new Api();
        this.state = {
            id: sessionStorage.getItem('id'),
            peerTyping: false,
            typingTimeout: Settings.chat.typingTimeout,
            activityTimeout: Settings.chat.activityTimeout,
            message: '',
            sessionId: this.props.session.id
        };

        this.activityTimeout();
        this.typingTimeout();
    }

    componentWillMount = () => {
        if (!this.state.sessionId) {
            this.props.router.push('/customer');
        }
    }

    componentDidMount = () => {

        let messages = this.props.session.transcript,
            typingTimer,
            keyCount,
            timeout = false;

        this.api.socket.emit('SECURE_CHAT_ROOM', this.props.session.id);

        this.api.socket.on('RECEIVE_AGENT_DETAILS', (data) => {
            this.props.actions.updateAgentPhoto(data.agentPhoto);
        });

        this.api.socket.on('BROADCAST_TYPING_STARTED', (data) => {
            this.setState({
                peerTyping: true
            });
        });

        this.api.socket.on('BROADCAST_TYPING_STOPPED', (data) => {
            this.setState({
                peerTyping: false
            });
        });

        this.api.socket.on('BROADCAST_MESSAGE', (data) => {
            messages = this.props.session.transcript;
            messages.push({
                'message': data.message,
                'created': data.createdAt,
                'agent': data.agent
            });
            // Update the redux store with the updated chat trascript. This is because the customer only retains a local copy, whilst the agent uses the API (for auth reasons)
            this.props.actions.updateChatTranscript(messages);
        });

        this.api.socket.on('DROP_PARTICIPANTS', () => {
            this.props.actions.terminateSession(this.state.sessionId);
            this.props.router.push('/customer/review');
        });

    }

    componentWillUnmount = () => {
        this.api.socket.disconnect(true);
        clearTimeout(this.activityTimer);
        clearTimeout(this.typingTimer);
    }

    endChat = () => {
        this.api.socket.emit('CHAT_TERMINATED', {
            sessionId: this.state.sessionId,
            timeout: this.timeout,
            isAgent: false
        });
        this.props.router.push('/customer/review');
      }

    activityTimeout = () => {
        this.activityTimer = setTimeout(function() {
            this.timeout = true;
            this.endChat();
        }.bind(this), this.state.activityTimeout);
    }

    typingTimeout = () => {
        if (this.keyCount === 1) {
          this.api.socket.emit('TYPING_STARTED', {
              sessionId: this.props.session.id
          });
        }
        this.typingTimer = setTimeout(function() {
            this.api.socket.emit('TYPING_STOPPED', {
               sessionId: this.props.session.id
            });
            this.keyCount = 0;
        }.bind(this), this.state.typingTimeout);
    }


    handleChange = (event) => {
        this.keyCount++;
        this.setState({
            message: event.target.value
        });

        clearTimeout(this.activityTimer);
        clearTimeout(this.typingTimer);

        this.activityTimeout();
        this.typingTimeout();
    }

    handleKeyPress = (event) => {
        if (event.key === 'Enter') {
            this.sendMessage();
        }
    }

    sendMessage = () => {
      if ( this.state.message.length > 0) {

        this.api.socket.emit('MESSAGE_SENT', {
            agent: false,
            message: this.state.message,
            sessionId: this.props.session.id
        });
        this.setState({
            message: '',
            selfTyping: false
        });

      } else {
         alert('Please enter a message');
      }

    }

    render = () => {

        return (

            <div>
                <div className="chat-wrapper">
                    <div className="chat-header">
                        <h4>Hello <b>{this.props.session.customerName}</b>, you are connected to <b>{this.props.session.agentName}</b></h4>
                        <div className="btn-group" role="group" aria-label="...">
                            <button onClick={this.endChat} className="btn btn-default" type="button">
                            <i className="material-icons">exit_to_app</i> Leave Chat
                            </button>
                        </div>
                    </div>
                    <div className="chat-content">
                        <MessageList
                        agentPhoto={ this.props.session.agentPhoto }
                        agentName={ this.props.session.agentName }
                        customerName={ this.props.session.customerName }
                        transcript={ this.props.session.transcript }
                        />
                    </div>
                    <div className="chat-footer">
                        <MessageBox
                        peerTyping={this.state.peerTyping}
                        handleChange={this.handleChange}
                        handleKeyPress={this.handleKeyPress}
                        message={this.state.message}
                        sendMessage={this.sendMessage}
                        peer={this.props.session.agentName}
                        agent={this.props.session.agent} />
                    </div>
                </div>
            </div>

        );
  }
}

CustomerChat.propTypes = {
  actions: PropTypes.object,
  dispatch: PropTypes.object,
  route: PropTypes.object
};

function mapStateToProps(state, ownProps) {
  return {
    session: state.session,
    auth: state.auth
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      initiateSession,
      terminateSession,
      updateAgentPhoto,
      updateAgentRole,
      updateChatTranscript
    }, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CustomerChat);