import React, { Component } from 'react';
import PropTypes from 'prop-types';

export class CustomerFinish extends Component {

  constructor(props) {
      super(props);
  }

  render = () => {

    return (

      <div className="container">
        <h3>All done!</h3>
      </div>

    );
  }
}

CustomerFinish.propTypes = {

};


export default CustomerFinish;