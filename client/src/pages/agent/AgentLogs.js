import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import { swal } from 'react-redux-sweetalert2';
import axios from 'axios';

import TabContainer from '../../components/Tabs/TabContainer';
import TabHeader from '../../components/Tabs/TabHeader';
import TabHeaderItem from '../../components/Tabs/TabHeaderItem';
import TabContent from '../../components/Tabs/TabContent';
import TabPane from '../../components/Tabs/TabPane';

import moment from 'moment';
import Api from '../../api/api';

const ROOT_URL = 'http://localhost:3000'; // This is the location of the sockets.io server

import { getLogs, getTranscript } from '../../actions/sessionActions';

export class AgentLogs extends Component {

  constructor(props) {
    super(props);
    this.state = {
      item: [],
      transcript: [],
      loaded: false
    };
    this.api = new Api();
  }

    componentWillMount = () => {
      this.props.actions.getLogs();
   }

  componentDidUpdate = () => {


console.log(this.state)

if (this.state.loaded) {

    const agentName = this.state.item.agentData.name,
          customerName = this.state.item.customerData.name,
          created = moment(this.state.item.createdAt),
          accepted = moment(this.state.item.acceptedAt),
          terminated = moment(this.state.item.terminatedAt),
          waitTime = moment.duration(created.diff(accepted)).humanize(),
          duration = moment.duration(accepted.diff(terminated)).humanize();

    const messages = this.state.transcript.map((item, i) => {
      const createdBy = item.agent ? agentName : customerName;
      return (
            <li key={i} style={{padding: '1rem 2rem', borderBottom: '1px solid #fff', background: item.agent ? '#efe8ff' : 'white'}}>
              <p><b><small className="text-muted">{createdBy} - {moment(item.createdAt).format('DD-MMM-YYYY h:mm:ss a')}</small></b><br/>
                {item.message}
              </p>
            </li>
       );
    });

    const overview = (
      `<p>Chat between ${agentName} and ${customerName}, ${moment(this.state.item.createdAt).calendar()}, Lasted ${duration}</p>
      <div className="chat-summary">
              <ul className="media-list">
                ${JSON.stringify(this.state.transcript, null, 4)}
              </ul>
      </div>`
    )


    let swalOptions = {
      title: 'Chat Log ' + this.state.item.sessionId,
      html: overview,
      width: 760
    };

    this.props.actions.showAlert(swalOptions);

      this.setState({
        loaded: false
      });


}



  }


 reviewChat(item) {

    // this.props.actions.getTranscript(item.sessionId);

     axios.get(`${ROOT_URL}/api/logs`, {
          headers: {
              authorization: localStorage.getItem('token')
          },
          params: {
              sessionId: item.sessionId
          }
      })
    .then((response) => {
      console.log('response', response.data[0].transcript)
      this.setState({
        item,
        transcript: response.data[0].transcript,
        loaded: true
      });
    })

  }

  render = () => {

    let logs;

    if (this.props.session.logs.length > 0) {

      let count = 0;
      logs = this.props.session.logs.map((item, i) => {

        if (item.terminated) { // does not show queue abandonments

          count++;

          const actions = (<a onClick={ () => {
             this.reviewChat(item);
           } } className="btn btn-fill btn-success">View</a>);

          const dateTime = moment(item.createdAt).calendar();
          const created = moment(item.createdAt);
          const accepted = moment(item.acceptedAt);
          const completed = moment(item.completedAt);

          const waitTime = moment.duration(created.diff(accepted)).humanize();
          const duration = moment.duration(accepted.diff(completed)).humanize();

          return (

            <tr key={ i }>
              <td className="text-center">
                { count }
              </td>
              <td>
                { item.customerData.name }
              </td>
              <td>
                { item.customerData.email }
              </td>
              <td>
                { item.agentData.name }
              </td>
              <td>
                { dateTime }
              </td>
              <td>
                { waitTime }
              </td>
              <td>
                { duration }
              </td>
              <td>
                { actions }
              </td>
            </tr>

            );
        }
      });

    }

    return (

      <div>
      <TabContainer>
        <TabHeader>
          <TabHeaderItem target="#all" active={true} icon="access_time" label="All" />
        </TabHeader>

        <TabContent>
          <TabPane active={true} id="all">{ logs }</TabPane>
        </TabContent>
      </TabContainer>
      </div>

      );
  }
}

AgentLogs.propTypes = {
  actions: PropTypes.object,
  dispatch: PropTypes.func,
  route: PropTypes.object
};


function mapStateToProps(state, ownProps) {
  return {
    message: state.auth.message,
    session: state.session
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      getLogs,
      getTranscript,
      ...swal
    }, dispatch)
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(AgentLogs);

