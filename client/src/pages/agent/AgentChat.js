import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { bindActionCreators } from 'redux';

import { updateAgentRole, terminateSession, getTranscript, updateAgentPhoto } from '../../actions/sessionActions';
import { swal } from 'react-redux-sweetalert2';

import { MessageBox, MessageList } from '../../components/Messages';

import Settings from '../../config/settings';
import Api from '../../api/api';

export class AgentChat extends Component {

  constructor(props) {
    super(props);
    this.api = new Api();
    this.state = {
      peerTyping: false,
      typingTimeout: Settings.chat.typingTimeout,
      message: ''
    };

    this.typingTimeout();
  }

  componentWillMount = () => {

    this.props.actions.getTranscript(this.props.session.id);
    this.props.actions.updateAgentPhoto(this.props.auth.profile.photo);
    this.props.actions.updateAgentRole(true);

    let messages = this.props.session.transcript,
        typingTimer,
        keyCount;

    if (!this.props.session.id) {
      this.props.router.push('/agent/queue');
    }

  }

  componentDidMount = () => {

    this.api.socket.emit('SECURE_CHAT_ROOM', this.props.session.id);

    this.api.socket.emit('BROADCAST_AGENT_DETAILS', {
      agentName: this.props.auth.profile.firstName,
      agentPhoto: this.props.auth.profile.photo
    });

    this.api.socket.on('BROADCAST_TYPING_STARTED', (data) => {
      this.setState({
        peerTyping: true
      });
    });

    this.api.socket.on('BROADCAST_TYPING_STOPPED', (data) => {
      this.setState({
        peerTyping: false
      });
    });

    this.api.socket.on('BROADCAST_MESSAGE', (data) => {
      this.props.actions.getTranscript(this.props.session.id);
    });

    this.api.socket.on('DROP_PARTICIPANTS', (data) => {
      this.showAlertMessage(data.isAgent ? 'You' : this.props.session.customerName);
      this.props.actions.terminateSession(this.props.session.id);
    });

  }

  componentWillUnmount = () => {
    let messages = [];
    clearTimeout(this.typingTimer);
    this.api.socket.disconnect(true);
  }

  showAlertMessage = (user) => {

    let swalOptions = {
      title: user + ' ended the chat',
      type: 'success'
    };

    this.props.actions.showAlert(swalOptions);
    this.props.router.push('/agent/dashboard');

  }

  endChat = () => {

    this.api.socket.emit('CHAT_TERMINATED', {
      sessionId: this.props.session.id,
      isAgent: true
    });

  }

  typingTimeout = () => {

    if (this.keyCount === 1) {
      this.api.socket.emit('TYPING_STARTED', {
        sessionId: this.props.session.id,
        agent: true
      });
    }

    this.typingTimer = setTimeout(function() {

      this.setState({
        selfTyping: false
      });

      this.api.socket.emit('TYPING_STOPPED', {
        sessionId: this.props.session.id,
        agent: true
      });

      this.keyCount = 0;

    }.bind(this), this.state.typingTimeout);
  }

  handleChange = (event) => {

    this.keyCount++;
    this.setState({
      message: event.target.value
    });

    clearTimeout(this.typingTimer);
    this.typingTimeout();
  }

  handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      this.sendMessage();
    }
  }

  cannedMessage = (event) => {
    this.setState({
      message: event.target.innerHTML
    });
  }

  sendMessage = () => {

    if (this.state.message.length > 0) {

      this.api.socket.emit('MESSAGE_SENT', {
        agent: true,
        message: this.state.message,
        sessionId: this.props.session.id
      });

      this.setState({
        message: ''
      });

    } else {
      alert('Please enter a message!');
    }

  }

  render = () => {

    const canned = [
      `Hi ${this.props.session.customerName}, I'm ${this.props.profile.firstName} how can I help you today?`,
      `Could you please give me your order number reference?`,
      `Thanks ${this.props.session.customerName}, just a minute please while I check some details...`,
      `That must be dissapointing, let me see what I can do to get this sorted for you`,
      `No problem. Thanks for contacting Claire's, have a great day`
    ];

    const responses = canned.map((item, i) => {
      return (
        <li key={ i } onClick={ this.cannedMessage }>{ item }</li>
      );
    });

    return (
      <div className="content">
      <div className="card">
        <div className="card-header" data-background-color="purple">
          <b>You are connected to <b>{ this.props.session.customerName }</b></b>
        </div>
        <div className="card-content agent-chat">
            <ul className="responses">
              { responses }
            </ul>

            <MessageBox peerTyping={ this.state.peerTyping } handleChange={ this.handleChange } handleKeyPress={ this.handleKeyPress } message={ this.state.message } sendMessage={ this.sendMessage }
              peer={ this.props.session.customerName } agent={ true } />
            <br/>

            <MessageList
              agentPhoto={ this.props.session.agentPhoto }
              agentName={ this.props.session.agentName }
              customerName={ this.props.session.customerName }
              transcript={ this.props.session.transcript }
            />
        </div>

        <div className="card-footer">
            <div className="btn-group" role="group" aria-label="...">
              <button onClick={ this.endChat } className="btn btn-default" type="button">
                <i className="material-icons">exit_to_app</i> Leave Chat
              </button>
            </div>
        </div>
      </div>

    </div>

      );
  }

}

AgentChat.propTypes = {
  actions: PropTypes.object
};

function mapStateToProps(state, ownProps) {
  return {
    session: state.session,
    auth: state.auth,
    profile: state.auth.profile
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      updateAgentRole,
      updateAgentPhoto,
      terminateSession,
      getTranscript,
      ...swal
    }, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AgentChat);


