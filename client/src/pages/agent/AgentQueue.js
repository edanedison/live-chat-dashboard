import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { bindActionCreators } from 'redux';

import TabContainer from '../../components/Tabs/TabContainer';
import TabHeader from '../../components/Tabs/TabHeader';
import TabHeaderItem from '../../components/Tabs/TabHeaderItem';
import TabContent from '../../components/Tabs/TabContent';
import TabPane from '../../components/Tabs/TabPane';

import {Helmet} from "react-helmet";
import moment from 'moment';
import Api from '../../api/api';

import { initiateSession, updateAgentQueue, suspendSession } from '../../actions/sessionActions';

export class AgentQueue extends Component {

  constructor(props) {
    super(props);
    this.api = new Api();
    this.state = {
      time: Date.now()
    };
  }

  componentWillMount = () => {
    // This will suspend the current active chat session to allow the agent to perhaps pick up another one?
    // this.props.actions.suspendSession();
    this.props.actions.updateAgentQueue();
  }

  componentDidMount = () => {

    // Updates the state every minute in order to force a re-render. This is to update the queue times without the need for a refresh.

    this.interval = setInterval(() => this.setState({
      time: Date.now()
    }), 60000);

    // Listens for socket connections / disconnections and dispatches an action to update the agent queue

    this.api.socket.on('UPDATE_CUSTOMER_QUEUE', () => {
      this.props.actions.updateAgentQueue();
    });

  }

  componentDidUpdate = () => {

    // If the store has an 'active session' flag, then the agent should be redirected to the chat page

    if (this.props.session.active) {

      sessionStorage.setItem('sessionId', this.props.session.id);

      this.api.socket.emit('INITIATE_CHAT', {
        sessionId: this.props.session.id,
        customerName: this.props.session.customerName,
        agentFirstName: this.props.profile.firstName,
        agentEmail: this.props.profile.email
      });

      this.props.router.push('/agent/chat');
    }

  }

  componentWillUnmount = () => {
    this.api.socket.disconnect(true);
    clearInterval(this.interval);
  }

  beginChat = (item) => {
    this.props.actions.initiateSession(
      item.sessionId,
      item.customerData.name,
      this.props.profile.firstName
    );
  }

  render = () => {


    let waiting,
      inProgress,
      completed,
      dropped,
      waitingCount;

    if (this.props.session.logs.length > 0) {

      waitingCount = 0;
      waiting = this.props.session.logs.map((item, i) => {

        if (!item.completed && !item.accepted && !item.dropped) {

          const actions = (
          waitingCount === 0 ? <a onClick={ () => {
               this.beginChat(item);
             } } className="btn btn-fill btn-success">Begin chat</a> : ''
          );

          waitingCount++;

          const dateTime = moment(item.createdAt).calendar(),
                waitTime = moment(item.createdAt).fromNow(),
                duration = '-';

          return (

            <tr key={ i }>

              <td className="text-center">
                { waitingCount }
              </td>
              <td>
                { item.customerData.name }
              </td>
              <td>
                { item.customerData.email }
              </td>
              <td>
                { item.agentData ? item.agentData.name : null }
              </td>
              <td>
                { dateTime }
              </td>
              <td>
                { waitTime }
              </td>
              <td>
                { duration }
              </td>
              <td>
                { actions }
              </td>
            </tr>

            );
        }
      });

      let inProgressCount = 0;
      inProgress = this.props.session.logs.map((item, i) => {

        const actions = (
        typeof item.agentData !== "undefined" && this.props.profile.email === item.agentData.email ? <a onClick={ () => {
               this.beginChat(item);
             } } className="btn btn-fill btn-success">Rejoin</a> : ''
        );

        if (item.accepted && !item.completed && !item.dropped) {

          inProgressCount++;

          const dateTime = moment(item.createdAt).calendar(),
                now = moment(new Date()),
                created = moment(item.createdAt),
                accepted = moment(item.acceptedAt),
                waitTime = moment.duration(created.diff(accepted)).humanize(),
                duration = moment.duration(accepted.diff(now)).humanize();

          return (

            <tr key={ i }>
              <td className="text-center">
                { inProgressCount }
              </td>
              <td>
                { item.customerData.name }
              </td>
              <td>
                { item.customerData.email }
              </td>
              <td>
                { item.agentData ? item.agentData.name : null }
              </td>
              <td>
                { dateTime }
              </td>
              <td>
                { waitTime }
              </td>
              <td>
                { duration }
              </td>
              <td>
                { actions }
              </td>
            </tr>

            );
        }
      });

      let completedCount = 0;
      completed = this.props.session.logs.map((item, i) => {

        const actions= (<a onClick={ () => {
               this.reviewChat(item.sessionId);
             } } className="btn btn-fill btn-success">View</a>);

        if (item.completed) {

          completedCount++;

          const dateTime = moment(item.createdAt).calendar(),
                created = moment(item.createdAt),
                accepted = moment(item.acceptedAt),
                completed = moment(item.completedAt),
                waitTime = moment.duration(created.diff(accepted)).humanize(),
                duration = moment.duration(accepted.diff(completed)).humanize();

          return (

            <tr key={ i }>
              <td className="text-center">
                { completedCount }
              </td>
              <td>
                { item.customerData.name }
              </td>
              <td>
                { item.customerData.email }
              </td>
              <td>
                { item.agentData ? item.agentData.name : null }
              </td>
              <td>
                { dateTime }
              </td>
              <td>
                { waitTime }
              </td>
              <td>
                { duration }
              </td>
              <td>
                { actions }
              </td>
            </tr>

            );
        }
      });

      let droppedCount = 0;
      dropped = this.props.session.logs.map((item, i) => {

        const actions = (<a onClick={ () => {
               this.reviewChat(item.sessionId);
             } } className="btn btn-fill btn-success">View</a>);

        if (item.dropped) {

          droppedCount++;

          const dateTime = moment(item.createdAt).calendar(),
                created = moment(item.createdAt),
                accepted = moment(item.acceptedAt),
                dropped = moment(item.droppedAt),
                waitTime = moment.duration(created.diff(accepted)).humanize(),
                duration = moment.duration(accepted.diff(completed)).humanize();

          return (

            <tr key={ i }>
              <td className="text-center">
                { droppedCount }
              </td>
              <td>
                { item.customerData.name }
              </td>
              <td>
                { item.customerData.email }
              </td>
              <td>
                { item.agentData ? item.agentData.name : null }
              </td>
              <td>
                { dateTime }
              </td>
              <td>
                { waitTime }
              </td>
              <td>
                { duration }
              </td>
              <td>
                { actions }
              </td>
            </tr>

            );
        }
      });

    }

    const title = waitingCount > 0 ? `Live Chat (${waitingCount})` : 'Live Chat';


    return (

      <div>
      <Helmet title={title} />
      <TabContainer>
        <TabHeader>
          <TabHeaderItem target="#waiting" active={true} icon="access_time" label="Waiting" />
          <TabHeaderItem target="#in-progress" icon="chat" label="In Progress" />
          <TabHeaderItem target="#completed" icon="check" label="Completed" />
          <TabHeaderItem target="#dropped" icon="trending_down" label="Dropped" />
        </TabHeader>

        <TabContent>
          <TabPane active={true} id="waiting">{ waiting }</TabPane>
          <TabPane id="in-progress">{ inProgress }</TabPane>
          <TabPane id="completed">{ completed }</TabPane>
          <TabPane id="dropped">{ dropped }</TabPane>
        </TabContent>
      </TabContainer>
      </div>


      );
  }
}

AgentQueue.propTypes = {
  actions: PropTypes.object,
  dispatch: PropTypes.func,
  route: PropTypes.object
};


function mapStateToProps(state, ownProps) {
  return {
    profile: state.auth.profile,
    message: state.auth.message,
    session: state.session
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      initiateSession,
      updateAgentQueue,
      suspendSession
    }, dispatch)
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(AgentQueue);

