import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchProfile } from '../../actions/authActions';
import DashTile from '../../components/DashTile';

export class Dashboard extends Component {

constructor(props) {
    super(props);
}


componentWillMount() {
    this.props.actions.fetchProfile();
}


  render() {

    return (

        <div className="content">
            <div className="container-fluid">
                <div className="row">
                    <DashTile value={this.props.stats.waiting} color="orange" label="Waiting" />
                    <DashTile value={this.props.stats.inProgress} color="blue" label="In progress" />
                    <DashTile value={this.props.stats.completed} color="green" label="Completed" />
                    <DashTile value={this.props.stats.dropped} color="red" label="Dropped" />
                </div>
            </div>
        </div>

    );
  }
}

function mapStateToProps(state) {
  return {
    session: state.session,
    profile: state.auth.profile,
    stats: state.session.stats
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
    fetchProfile
    }, dispatch)
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);

