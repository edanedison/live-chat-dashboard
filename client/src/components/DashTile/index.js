import React, { Component } from 'react';
import PropTypes from 'prop-types';
import reactCSS from 'reactcss';

const DashTile = ({...props}) => {


  return (
  <div className="col-md-6">
      <div className="card card-chart">
          <div className="card-header" data-background-color={props.color} style={{ 'display': 'flex', 'alignItems': 'center', 'justifyContent': 'center', 'fontSize': '15rem', minHeight: '250px'}}>
              <div>{props.value}</div>
          </div>
          <div className="card-content">
              <h4 className="card-title">{props.label}</h4>
          </div>
      </div>
  </div>
  );
};

DashTile.propTypes = {
  value: PropTypes.number,
  label: PropTypes.string,
  color: PropTypes.string
};


export default DashTile;


