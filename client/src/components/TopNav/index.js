import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';

class topNav extends Component {

  renderAccount = () => {
    if (this.props.authenticated) {
      return(
       <ul className="nav navbar-nav navbar-right">
              <li className="dropdown">
                <a href="#" className="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                 <i className="material-icons">person</i>
                      <p className="hidden-lg hidden-md">
                          Settings
                          <b className="caret"></b>
                      </p>
                  <div className="ripple-container"></div></a>
                <ul className="dropdown-menu">
                  <li>
                    <a href="#">Edit Profile</a>
                  </li>
                  <li>
                    <a href="#">Settings</a>
                  </li>
                  <li>
                    <Link to="/signout">Sign Out</Link>
                  </li>
                </ul>
              </li>
            </ul>
      );
    }
  }

  render = () => {

  return (

      <nav className="navbar navbar-transparent navbar-absolute">
        <div className="container-fluid">
          <div className="navbar-minimize">
            <button id="minimizeSidebar" className="btn btn-round btn-white btn-fill btn-just-icon">
              <i className="material-icons visible-on-sidebar-regular">chevron_left</i>
              <i className="material-icons visible-on-sidebar-mini">chevron_right</i>
            </button>
          </div>
          <div className="navbar-header">
            <button type="button" className="navbar-toggle" data-toggle="collapse">
              <span className="sr-only">Toggle navigation</span>
              <span className="icon-bar" />
              <span className="icon-bar" />
              <span className="icon-bar" />
            </button>
            <a className="navbar-brand" href="#"><b>{ this.props.profile.firstName } { this.props.profile.lastName }</b></a>
          </div>
          <div className="collapse navbar-collapse">
            { this.renderAccount() }
          </div>
        </div>
      </nav>

      );
  }
}

function mapStateToProps(state) {
  return {
    authenticated: state.auth.authenticated,
    profile: state.auth.profile,
    session: state.session
  };
}

export default connect(mapStateToProps)(topNav);
