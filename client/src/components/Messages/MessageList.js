import React, { Component } from 'react';
import PropTypes from 'prop-types';
import reactCSS from 'reactcss';

import MessageItem from './MessageItem';

const MessageList = ({...props}) => {

  const messages = (
      props.transcript.map(function(item, i){
        return <MessageItem {...props} {...item} key={i} />;
      })
  );

  return (
    <ul className="media-list">
      {messages}
    </ul>
  );
};

MessageList.propTypes = {
  session: PropTypes.array,
  transcript: PropTypes.array
};

export default MessageList;

