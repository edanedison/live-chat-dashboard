import React, { Component } from 'react';
import PropTypes from 'prop-types';
import reactCSS from 'reactcss';
import moment from 'moment';

const MessageItem = ({...props}) => {

  const since = moment(props.message.createdAt).fromNow(),
        user = props.agent ? props.agentName : props.customerName,
        photo = props.agent ? props.agentPhoto : '/img/default-avatar.png';

  return (

    <li className="media">
        <div className="media-body">
            <div className="media">
                <a className="pull-left" href="#">
                    <img className="media-object img-circle" width="48" src={photo}/>
                </a>
                <div className="media-body">
                    {props.message}
                    <br/>
                      <b><small className="text-muted">{user} | {since}</small></b>
                    <hr/>
                </div>
            </div>
        </div>
    </li>

  );
};

MessageItem.propTypes = {
  customerName: PropTypes.string,
  agentName: PropTypes.string,
  message: PropTypes.string,
  createdAt: PropTypes.node
};


export default MessageItem;
