import React, { Component } from 'react';
import PropTypes from 'prop-types';
import reactCSS from 'reactcss';

const MessageBox = ({...props}) => {

  const groupClass = (
      !props.message ? 'form-group label-floating is-empty' : 'form-group label-floating'
  );

  return (
        <div>
            <div className={groupClass}>
              <label className="control-label">Your message</label>
              <input value={props.message} onChange={props.handleChange} onKeyPress={props.handleKeyPress} type="text" className="form-control" />
              <span className="material-input" />
            </div>

            <button type="submit" className="btn btn-fill btn-success btn-lg" onClick={props.sendMessage}>Send Message</button>
            &nbsp;&nbsp;&nbsp;{props.peerTyping ? `${props.peer} is typing...` : null}
        </div>
  );
};

MessageBox.propTypes = {
  message: PropTypes.string
};

export default MessageBox;
