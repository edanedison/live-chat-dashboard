import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { bindActionCreators } from 'redux';
import moment from 'moment';
import { updateAgentAvailability, updateAgentCount } from '../../actions/sessionActions';
import NavLink from "../NavLink";
import AgentStatus from "./AgentStatus";
import Api from '../../api/api';

class SideNav extends Component {

  constructor(props) {
      super(props);
      this.api = new Api();
      this.props.actions.updateAgentAvailability(JSON.parse(sessionStorage.getItem('available')));
  }

  componentWillMount = () => {
    if (!this.props.auth.authenticated) {
      this.props.router.push('/agent/signin');
    }
  }

  handleChange = (e) => {

      sessionStorage.setItem('available', e.target.checked);
      this.props.actions.updateAgentAvailability(e.target.checked);
      this.props.actions.updateAgentCount();

      this.api.socket.emit('AGENT_STATUS_CHANGE', {
          username: this.props.profile.email,
          status: e.target.checked
      });
  }

  renderCurrentChat = () => {
    if (this.props.session.active) {
      return(
        <NavLink to="/agent/chat">
            <i className="material-icons">chat_bubble</i>
            <p>{this.props.session.customerName}</p>
        </NavLink>
      );
    }
  }

  render = () => {

    const currentDate = moment().format("ddd Do MMMM YYYY");

      return (

        <div>
          <AgentStatus
            date={currentDate}
            available={this.props.session.agentAvailable}
            toggleClick={this.handleChange}
            agentCount={this.props.session.agentCount}
           />
          <div style={{'padding': '0 2rem'}}>
             <img width="100%" src={this.props.profile.photo} style={ { opacity: this.props.session.agentAvailable ? 1 : 0.5 } }/>
          </div>
          <ul className="nav" onClick={this.props.onNavChange}>
            {this.renderCurrentChat()}
            <NavLink to="/agent/dashboard"><i className="material-icons">home</i><p>Dashboard</p></NavLink>
            <NavLink to="/agent/queue"><i className="material-icons">list</i><p>Chat Session</p></NavLink>
            <NavLink to="/agent/logs"><i className="material-icons">history</i><p>Logs</p></NavLink>
          </ul>
       </div>
      );
    }
}

function mapStateToProps(state) {
  return {
    auth: state.auth,
    profile: state.auth.profile,
    session: state.session
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      updateAgentAvailability,
      updateAgentCount
    }, dispatch)
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(SideNav);

