import React, { Component } from 'react';
import PropTypes from 'prop-types';

const AgentStatus = ({...props}) => {

  return (
  <div className="user" style={{'textAlign': 'left',padding: '0 2rem 1rem'}}>
            <h4 className="visible-on-sidebar-regular"><b>{props.date}</b></h4>
            <p>
              <b><span className="visible-on-sidebar-regular">You are</span> {props.available ? 'Online' : 'Offline'}</b>
            </p>
            <div className="togglebutton" style={{marginBottom: '1rem'}}>
            <label>
              <input onChange={props.toggleClick} type="checkbox" checked={props.available || false} />
            </label>
            </div>
            <p className="visible-on-sidebar-regular">
              <b>{props.agentCount} agent(s) available</b>
            </p>
  </div>
  );
};

AgentStatus.propTypes = {
  message: PropTypes.string
};

export default AgentStatus;
