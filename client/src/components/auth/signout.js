import React, { Component } from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import * as actions from '../../actions/authActions';

class Signout extends Component {
  componentWillMount() {
    this.props.signoutUser();
  }

  componentDidMount() {
    browserHistory.push('/signin');
  }

  render() {
    return <div>You have been logged out.</div>;
  }
}

export default connect(null, actions)(Signout);
