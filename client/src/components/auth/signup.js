import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import { Link, browserHistory } from 'react-router';
import * as actions from '../../actions/authActions';

class Signup extends Component {


  componentDidMount() {
    if (this.props.authenticated) {
      browserHistory.push('/agent/queue');
    }

    this.refs.card.className="card card-login";

  }

  handleFormSubmit(formProps) {
    this.props.signupUser(formProps);
  }

  renderAlert() {
    if (this.props.errorMessage) {
      return (
        <h4 className="text-center">
          <b>{this.props.errorMessage}</b>
        </h4>
      );
    }
  }

  render() {
      const { handleSubmit, fields: { firstName, email, password, passwordConfirm }} = this.props;

    return (
      <div>
 <nav className="navbar navbar-primary navbar-transparent navbar-absolute">
        <div className="container">
            <div className="navbar-header">
                <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                    <span className="sr-only">Toggle navigation</span>
                    <span className="icon-bar"></span>
                    <span className="icon-bar"></span>
                    <span className="icon-bar"></span>
                </button>

            <div className="logo">
               <a href="/" className="simple-text">
                <img width="200" src="/img/logo.svg" />
              </a>
            </div>
                <a className="navbar-brand" href=" ../dashboard.html ">Claire's Customer Service Dashboard</a>


            </div>
            <div className="collapse navbar-collapse">
                <ul className="nav navbar-nav navbar-right">
                    <li className="active">
                      <Link to="/signup"><i className="material-icons">person_add</i> Register</Link>
                    </li>
                    <li className="">
                        <Link to="/signin"><i className="material-icons">fingerprint</i> Login</Link>
                    </li>

                </ul>
            </div>
        </div>
    </nav>
    <div className="wrapper wrapper-full-page">
        <div className="full-page login-page">
            <div className="content">
                <div className="container">
                    <div className="row">
                        <div className="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                            <form className="form-horizontal" onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
                                <div ref="card" className="card card-login card-hidden">
                                    <div className="card-header text-center" data-background-color="tertiary">
                                        <h4 className="card-title">Agent Registration</h4>

                                    </div>

                                    <div className="card-content">

                                                 {this.renderAlert()}


                                        <div className="input-group">
                                            <span className="input-group-addon">
                                                <i className="material-icons">person</i>
                                            </span>
                                            <div className={`form-group label-floating is-empty ${firstName.touched && firstName.error ? 'has-error' : ''}`}>
                                                <label className="control-label">First name</label>
                                                <input {...firstName} className={firstName.touched && firstName.error ? 'form-control error' : 'form-control'} />
                                            </div>
                                        </div>


                                        <div className="input-group">
                                            <span className="input-group-addon">
                                                <i className="material-icons">email</i>
                                            </span>
                                            <div className={`form-group label-floating is-empty ${email.touched && email.error ? 'has-error' : ''}`}>
                                                <label className="control-label">Email</label>
                                                <input {...email} className={email.touched && email.error ? 'form-control error' : 'form-control'} />
                                            </div>
                                        </div>

                                        <div className="input-group">
                                            <span className="input-group-addon">
                                                <i className="material-icons">lock</i>
                                            </span>
                                            <div className={`form-group label-floating is-empty ${password.touched && password.error ? 'has-error' : ''}`}>
                                                <label className="control-label">Password</label>
                                                <input {...password} className={password.touched && password.error ? 'form-control error' : 'form-control'} />
                                            </div>
                                        </div>

                                        <div className="input-group">
                                            <span className="input-group-addon">
                                                <i className="material-icons" style={{visibility:'hidden'}}>lock</i>
                                            </span>
                                            <div className={`form-group label-floating is-empty ${passwordConfirm.touched && passwordConfirm.error ? 'has-error' : ''}`}>
                                                <label className="control-label">Confirm password</label>
                                                <input {...passwordConfirm} className={passwordConfirm.touched && passwordConfirm.error ? 'form-control error' : 'form-control'} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="footer text-center">
                                        <button type="submit" className="btn btn-lg btn-rose">Register</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <footer className="footer">
                <div className="container">
                    <nav className="pull-left">
                        <ul>
                            <li>
                                <a href="#">
                                    Claires
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Icing
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Style book
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <p className="copyright pull-right">
                        Copyright &copy; Claire's Accessories 2017. All rights reserved
                    </p>
                </div>
            </footer>
            <div className="full-page-background" style={{'backgroundImage': 'url(img/bg.jpg)'}}></div>
        </div>
    </div>
</div>


    );
  }
}

function validate(formProps) {
  const errors = {};

  if (!formProps.firstName) {
    errors.firstName = 'Please enter your first name';
  }

  if (!formProps.email) {
    errors.email = 'Please enter your email address';
  }

  if (!formProps.password) {
    errors.password = 'Please choose a password';
  }

  if (!formProps.passwordConfirm) {
    errors.passwordConfirm = 'Please confirm your password';
  }

  if (formProps.password !== formProps.passwordConfirm) {
    errors.password = 'Passwords must match';
  }

  return errors;
}

function mapStateToProps(state) {
  return { errorMessage: state.auth.error };
}

export default reduxForm({
  form: 'signup',
  fields: ['firstName', 'email', 'password', 'passwordConfirm'],
  validate
}, mapStateToProps, actions)(Signup);