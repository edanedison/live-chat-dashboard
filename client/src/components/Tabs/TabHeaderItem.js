import React, { Component } from 'react';
import PropTypes from 'prop-types';

const TabHeaderItem = ({...props}) => {

  return (

        <li className={props.active ? 'active' : ''}>
          <a href={props.target} data-toggle="tab" aria-expanded="true">
            <i className="material-icons">{props.icon}</i> {props.label}
            <div className="ripple-container" />
          </a>
        </li>

  );
};

TabHeaderItem.propTypes = {
};

export default TabHeaderItem;

