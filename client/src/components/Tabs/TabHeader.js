import React, { Component } from 'react';
import PropTypes from 'prop-types';

const TabHeader = ({...props}) => {

  return (

          <div className="card-header card-header-tabs" data-background-color="purple">
            <div className="nav-tabs-navigation">
              <div className="nav-tabs-wrapper">
                <ul className="nav nav-tabs" data-tabs="tabs">

                {props.children}

                </ul>
              </div>
            </div>
          </div>

  );
};

TabHeader.propTypes = {
};

export default TabHeader;

