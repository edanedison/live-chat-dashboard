import React, { Component } from 'react';
import PropTypes from 'prop-types';

const TabContainer = ({...props}) => {

  return (

      <div className="content">
        <div className="card">
          {props.children}
        </div>
      </div>

  );
};

TabContainer.propTypes = {
};

export default TabContainer;

