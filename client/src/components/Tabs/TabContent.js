import React, { Component } from 'react';
import PropTypes from 'prop-types';

const TabContent = ({...props}) => {

  return (

          <div className="card-content">
            <div className="tab-content">
            {props.children}
            </div>
          </div>

  );
};

TabContent.propTypes = {
};

export default TabContent;

