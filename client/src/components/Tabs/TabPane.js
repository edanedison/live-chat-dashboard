import React, { Component } from 'react';
import PropTypes from 'prop-types';

const TabPane = ({...props}) => {

  return (

              <div className={props.active ? 'tab-pane active' : 'tab-pane '} id={props.id}>
                <div className="card">
                  <div className="card-content">
                    <table className="table table-striped table-no-bordered table-hover">
                      <thead>
                        <tr>
                          <th className="text-center">#</th>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Agent</th>
                          <th>Created</th>
                          <th>Wait time</th>
                          <th>Chat time</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        {props.children}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

  );
};

TabPane.propTypes = {
};

export default TabPane;

