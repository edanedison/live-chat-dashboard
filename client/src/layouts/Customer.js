import React, { Component } from 'react';
import PropTypes from 'prop-types';
import settings from '../config/settings';

import Api from '../api/api';


class Customer extends Component {

    constructor(props) {
        super(props);
        this.api = new Api();
    }



  componentDidMount() {

      this.api.socket.emit('CHECK_CURRENT_SESSION', {
          sessionId: sessionStorage.getItem('sessionId'),
          user: 'Customer'
      });

  }



  render() {

    return (
      <div>
      <br/>
        {this.props.children}
      </div>
      );
  }
}

export default Customer;
