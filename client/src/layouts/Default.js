import React, { Component } from 'react';
import PropTypes from 'prop-types';
import settings from '../config/settings';


class Default extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      spinner: false
    };
  }

  onLoading() {
    return this.setState({
      spinner: true
    });
  }

  render() {

    return (
      <div>
        {this.props.children}
      </div>
      );
  }
}

export default Default;
