import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import moment from 'moment';
import { swal } from 'react-redux-sweetalert2';

import Notifier from 'react-desktop-notification';
import {Helmet} from "react-helmet";

import { updateCurrentStats, updateAgentCount } from '../actions/sessionActions';
import { fetchProfile } from '../actions/authActions';

import SideNav from '../components/SideNav';
import TopNav from '../components/TopNav';

import Api from '../api/api';

export class Agent extends Component {

  constructor(props) {
    super(props);
    this.api = new Api();
    this.state = {
        show: true
    };

    this.updateDashboard = ::this.updateDashboard;
    this.showAlertMessage = ::this.showAlertMessage;

  }

  componentWillMount() {
    this.props.actions.updateCurrentStats();
    this.props.actions.fetchProfile();
    this.props.actions.updateAgentCount();
  }

  componentDidMount() {

    this.updateDashboard();

  }

  updateDashboard() {

    this.api.socket.on('UPDATE_CURRENT_STATS', () => {
      this.props.actions.updateCurrentStats();
    });

    this.api.socket.on('UPDATE_AVAILABLE_AGENTS', () => {
      this.props.actions.updateAgentCount();
    });

    this.api.socket.on('NEW_CUSTOMER_ALERT', (data) => {

      Notifier.start(
        "Live chat alert",
        data.customerName + " has just joined the queue",
        "http://localhost:8080/agent/queue"
      );

    });

    this.api.socket.on('DROP_PARTICIPANTS', () => {
      this.props.actions.updateCurrentStats();
    });

  }

  showAlertMessage(message) {

    let swalOptions = {
      title: 'Enable desktop notifications?',
      text: message,
      type: 'success'
    };

    this.props.actions.showAlert(swalOptions);

    // <button onClick={() => this.showAlertMessage() } >Show Alert!</button>

  }


  render() {


    const currentDate = moment().format("dddd Do MMMM YYYY");


    return (
        <div className="wrapper">
          <div className="sidebar" data-active-color="rose" data-background-color="quaternary" data-image="">
            <div className="logo">
              <a href="/" className="simple-text">
                <img width="100%" src="/img/logo.svg" />
              </a>
            </div>
            <div className="logo logo-mini" style={ { 'padding': 10 } }>
              <a href="/">
                <img width="100%" src="/img/logo_small.png" />
              </a>
            </div>
            <div className="sidebar-wrapper">
              <SideNav onNavChange={ this.updateDashboard } />
            </div>
          </div>
          <div className="main-panel">
            <TopNav date={currentDate} />
            <div className="content">
              { this.props.children }
            </div>
          </div>
        </div>
      );
  }
}

function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      updateCurrentStats,
      fetchProfile,
      updateAgentCount,
      ...swal
    }, dispatch)
  };
}



export default connect(mapStateToProps, mapDispatchToProps)(Agent);