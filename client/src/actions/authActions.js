import axios from 'axios';
import { browserHistory } from 'react-router';
import * as types from './types';

// import * as types from './types';

const jwt_decode = require('jwt-decode');

const ROOT_URL = 'http://localhost:3000';


export function signinUser({ email, password }) {
  return function(dispatch) {
    // Submit email/password to the server
    axios.post(`${ROOT_URL}/signin`, { email, password })
      .then(response => {
        // If request is good...
        // - Update state to indicate user is authenticated
        dispatch({
          type: types.AUTH_USER
        });

        // decode token for info on the user
        let decoded_token_data = jwt_decode(response.data.token);

        console.log(response.data.token);

        // - Save the JWT token
        localStorage.setItem('token', response.data.token);

        // Remove the agent availability session token
        sessionStorage.setItem('available', false);

        // - redirect to the appropriate route
        if(decoded_token_data.role == 'agent') {
          browserHistory.push('/agent/dashboard');
        }
        // - set admin flag if token indicates the user has admin privileges
        else if(decoded_token_data.role == 'admin') {
          dispatch({ type: types.SET_ADMIN_PRIVILEGES });
          browserHistory.push('/agent/admin');
        }
        else {
          browserHistory.push('/');
        }
      })
      .catch(() => {
        // If request is bad...
        // - Show an error to the user
        dispatch(authError('Bad Login Info'));
      });
  };
}

export function signupUser({ firstName, lastName, email, password }) {
  return function(dispatch) {
    axios.post(`${ROOT_URL}/signup`, { firstName, lastName, email, password })
      .then(response => {
        dispatch({
          type: types.AUTH_USER,
          value: response.data.profile
        });
        localStorage.setItem('token', response.data.token);
        sessionStorage.setItem('available', false);
        browserHistory.push('/agent/dashboard');
      })
      .catch(response => dispatch(authError(response.data.error)));
  };
}

//token included in the header of the request for authorization
export function activateAdmin({ firstName, lastName, email, password }) {
  return function(dispatch) {
    axios.post(`${ROOT_URL}/admin/admin_activation`,
      { email, password },
      {headers: { authorization: localStorage.getItem('token') }} )
      .then(response => {
        browserHistory.push('/admin/admin_area');
      })
      .catch(response => dispatch(authError(response.data.error)));
  };
}


export function authError(error) {
  return {
    type: types.AUTH_ERROR,
    value: error
  };
}

export function signoutUser() {
  localStorage.removeItem('token');
  return { type: types.UNAUTH_USER };
}

//token included in the header of the request for authorization
export function fetchMessage() {
  return function(dispatch) {
    axios.get(`${ROOT_URL}/agent/dashboard`, {
      headers: { authorization: localStorage.getItem('token') }
    })
      .then(response => {
        dispatch({
          type: types.FETCH_MESSAGE,
          value: response.data.message
        });
      });
  };
}


//token included in the header of the request for authorization
export function fetchProfile() {
  return function(dispatch) {
    axios.get(`${ROOT_URL}/profile`, {
      headers: { authorization: localStorage.getItem('token') }
    })
      .then(response => {
        dispatch({
          type: types.UPDATE_PROFILE,
          value: response.data
        });
      });
  };
}


//token included in the header of the request for authorization
export function fetchAdminMessage() {
  return function(dispatch) {
    axios.get(`${ROOT_URL}/admin/admin_area`, {
      headers: { authorization: localStorage.getItem('token') }
    })
      .then(response => {
        dispatch({
          type: types.FETCH_ADMIN_MESSAGE,
          value: response.data.message
        });
      });
  };
}
