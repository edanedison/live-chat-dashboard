// Auth actions

export const AUTH_USER = 'AUTH_USER';
export const UNAUTH_USER = 'UNAUTH_USER';
export const AUTH_ERROR = 'AUTH_ERROR';
export const FETCH_MESSAGE = 'FETCH_MESSAGE';
export const FETCH_ADMIN_MESSAGE = 'FETCH_ADMIN_MESSAGE';
export const SET_ADMIN_PRIVILEGES = 'SET_ADMIN_PRIVILEGES';

// Session actions

export const INITIATE_SESSION_SUCCESS = 'INITIATE_SESSION_SUCCESS';
export const TERMINATE_SESSION_SUCCESS = 'TERMINATE_SESSION_SUCCESS';
export const SUSPEND_SESSION_SUCCESS = 'SUSPEND_SESSION_SUCCESS';

export const UPDATE_AGENT_AVAILABILITY = 'UPDATE_AGENT_AVAILABILITY';
export const UPDATE_AGENT_QUEUE = 'UPDATE_AGENT_QUEUE';
export const UPDATE_AGENT_ROLE = 'UPDATE_AGENT_ROLE';
export const UPDATE_AGENT_COUNT = 'UPDATE_AGENT_COUNT';

export const UPDATE_QUEUE_POSITION = 'UPDATE_QUEUE_POSITION';

export const UPDATE_CUSTOMER_ID_SUCCESS = 'UPDATE_CUSTOMER_ID_SUCCESS';
export const UPDATE_TRANSCRIPT_SUCCESS = 'UPDATE_TRANSCRIPT_SUCCESS';
export const UPDATE_STATS_SUCCESS = 'UPDATE_STATS_SUCCESS';
export const UPDATE_SESSION_PHOTO_SUCCESS = 'UPDATE_SESSION_PHOTO_SUCCESS';

export const GET_LOGS_SUCCESS = 'GET_LOGS_SUCCESS';
export const GET_TRANSCIPT_SUCCESS = 'GET_TRANSCIPT_SUCCESS';

// Profile actions

export const UPDATE_PROFILE = 'UPDATE_PROFILE';


