import axios from 'axios';
import * as types from './types';

const ROOT_URL = 'http://localhost:3000'; // This is the location of the sockets.io server

export function initiateSession(sessionId, customerName, agentName) {
    return (dispatch) => {
        dispatch({
            type: types.INITIATE_SESSION_SUCCESS,
            value: {
                sessionId,
                customerName,
                agentName
            }
        });
    };
}

export function terminateSession(data) {
    return (dispatch) => {
        dispatch({
            type: types.TERMINATE_SESSION_SUCCESS,
            value: data
        });
    };
}

export function suspendSession(data) {
    return (dispatch) => {
        dispatch({
            type: types.SUSPEND_SESSION_SUCCESS,
            value: data
        });
    };
}

export function updateAgentRole(data) {
    return (dispatch) => {
        dispatch({
            type: types.UPDATE_AGENT_ROLE,
            value: data
        });
    };
}

export function updateAgentPhoto(data) {
    return (dispatch) => {
        dispatch({
            type: types.UPDATE_SESSION_PHOTO_SUCCESS,
            value: data
        });
    };
}

export function updateAgentAvailability(data) {
    return (dispatch) => {
        dispatch({
            type: types.UPDATE_AGENT_AVAILABILITY,
            value: data
        });
    };
}

export function updateCustomerId(data) {
    return (dispatch) => {
        dispatch({
            type: types.UPDATE_CUSTOMER_ID_SUCCESS,
            value: data
        });
    };
}

export function updateChatTranscript(data) {
    return (dispatch) => {
        dispatch({
            type: types.UPDATE_TRANSCRIPT_SUCCESS,
            value: data
        });
    };
}

export function getLogs() {
    return function(dispatch) {
        axios.all([
                axios.get(`${ROOT_URL}/api/logs`, {
                    headers: {
                        authorization: localStorage.getItem('token')
                    }
                })
            ])
            .then(axios.spread(function(response) {
                dispatch({
                    type: types.GET_LOGS_SUCCESS,
                    value: response.data
                });
            }));
    };
}

export function getTranscript(data) {
    return function(dispatch) {
        axios.all([
                axios.get(`${ROOT_URL}/api/logs`, {
                    headers: {
                        authorization: localStorage.getItem('token')
                    },
                    params: {
                        sessionId: data
                    }
                })
            ])
            .then(axios.spread(function(response) {
                dispatch({
                    type: types.GET_TRANSCIPT_SUCCESS,
                    value: response.data[0].transcript
                });
            }));
    };
}

export function updateAgentQueue() {
    return function(dispatch) {
        axios.all([
                axios.get(`${ROOT_URL}/api/sessions`, {
                    headers: {
                        authorization: localStorage.getItem('token')
                    }
                })
            ])
            .then(axios.spread(function(response) {
                dispatch({
                    type: types.UPDATE_AGENT_QUEUE,
                    value: response.data
                });
            }));
    };
}

export function updateAgentCount() {
    return function(dispatch) {
        axios.all([
                axios.get(`${ROOT_URL}/api/agents/online`)
            ])
            .then(axios.spread(function(response) {
                dispatch({
                    type: types.UPDATE_AGENT_COUNT,
                    value: response.data
                });
            }));
    };
}

export function updateQueuePosition(sessionId) {
    return function(dispatch) {
        axios.all([
                axios.get(`${ROOT_URL}/api/sessions`, {
                    headers: {
                        authorization: localStorage.getItem('token')
                    },
                    params: {
                        accepted: false,
                        dropped: false,
                        completed: false
                    }
                })
            ])
            .then(axios.spread(function(response) {

                const queue = response;
                let position = 0;

                for (let i = 0; i < queue.data.length; ++i) {
                    const item = queue.data[i];
                    if (item.sessionId === sessionId) {
                        position = i + 1;
                    }
                }

                dispatch({
                    type: types.UPDATE_QUEUE_POSITION,
                    value: {
                        position
                    }
                });
            }));
    };
}

// Stats for current day or session

export function updateCurrentStats() {
    return function(dispatch) {
        axios.all([
                axios.get(`${ROOT_URL}/api/sessions`, {
                    headers: {
                        authorization: localStorage.getItem('token')
                    },
                    params: {
                        accepted: false,
                        dropped: false,
                        completed: false
                    }
                }),
                axios.get(`${ROOT_URL}/api/sessions`, {
                    headers: {
                        authorization: localStorage.getItem('token')
                    },
                    params: {
                        accepted: true,
                        dropped: false,
                        completed: false
                    }
                }),
                axios.get(`${ROOT_URL}/api/sessions`, {
                    headers: {
                        authorization: localStorage.getItem('token')
                    },
                    params: {
                        accepted: true,
                        dropped: false,
                        completed: true
                    }
                }),
                axios.get(`${ROOT_URL}/api/sessions`, {
                    headers: {
                        authorization: localStorage.getItem('token')
                    },
                    params: {
                        dropped: true
                    }
                })
            ])
            .then(axios.spread(function(waitingResponse, inProgressResponse, completedResponse, droppedResponse) {
                dispatch({
                    type: types.UPDATE_STATS_SUCCESS,
                    value: {
                        waiting: waitingResponse,
                        inProgress: inProgressResponse,
                        completed: completedResponse,
                        dropped: droppedResponse
                    }
                });
            }));
    };
}