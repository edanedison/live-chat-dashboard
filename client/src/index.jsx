import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import { persistStore, autoRehydrate } from 'redux-persist';
import { Router, browserHistory } from 'react-router';
import thunk from 'redux-thunk';
import Routes from './config/routes';
import jwt_decode from 'jwt-decode';
import SweetAlert from 'react-redux-sweetalert2';
import {Helmet} from "react-helmet";
import { AUTH_USER } from './actions/types';
import { SET_ADMIN_PRIVILEGES } from './actions/types';

import reducers from './reducers';

import 'jquery/src/jquery'; // Supports for the material UI interface that the app uses
import '../assets/js/externals.js';
import '../assets/sass/main.scss';

const enhancer = compose(
  autoRehydrate(),
  applyMiddleware(thunk),
  window.devToolsExtension ? window.devToolsExtension() : f => f
);


export const store = createStore(
  reducers,
  enhancer
);

const token = localStorage.getItem('token');
// update application state with token information if needed
if (token) {
  // update authentication flag
  store.dispatch({
    type: AUTH_USER
  });

  // update admin privileges if needed
  let decoded_token = jwt_decode(token);
  if (decoded_token.role == 'admin') {
    store.dispatch({
      type: SET_ADMIN_PRIVILEGES
    });
  }
}

// Get the state
const getState = () => {
  // console.log('Current store', store.getState());
};


const rootEl = document.getElementById('root');

ReactDOM.render(
  <Provider store={ store }>
    <div>
      <Helmet>
          <meta charSet="utf-8" />
          <title>Live Chat</title>
      </Helmet>
      <Router routes={ Routes(store) } history={ browserHistory } />
      <SweetAlert />
    </div>
  </Provider>
  , rootEl);


// Subscribe to store changes
store.subscribe(getState);

// persistStore(store)

