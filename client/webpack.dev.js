const path = require('path');
const DashboardPlugin = require('webpack-dashboard/plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
    cache: true,
    entry: './src/index.jsx',
    output: {
        path: path.join(__dirname, '/public'),
        publicPath: '/public',
        filename: 'bundle.js'
    },
    module: {
        loaders: [
        {
            test: /\.jsx?$/,
            include: path.join(__dirname, 'src'),
            exclude: /node_modules/,
            loader: "babel-loader",
            query: {
                presets: ['es2015', 'react', 'stage-0', 'stage-3']
            }
        },
          {
            test: /\.css$/,
            use: [ 'style-loader', 'css-loader' ]
          },
        {
            test: /\.scss$/,
            exclude: /node_modules/,
            use: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: [
                    {
                        loader: 'css-loader',
                        query: {
                            modules: true,
                            sourceMap: true,
                            importLoaders: 1,
                            localIdentName: '[local]',
                        }
                    },
                    'sass-loader?sourceMap'
                ]
            }),
        },
        {
            test: /\.(woff|woff2|eot|ttf|svg)$/,
            exclude: /node_modules/,
            loader: 'file-loader?limit=1024&name=fonts/[name].[ext]'
        },
        {
            test: /\.(jpe?g|png|gif|svg)$/i,
            loaders: ['file-loader?name=/css/images/[name].[ext]', {
                loader: 'image-webpack-loader',
                query: {
                    mozjpeg: {
                        progressive: true
                    },
                    gifsicle: {
                        interlaced: false
                    },
                    optipng: {
                        optimizationLevel: 4
                    },
                    pngquant: {
                        quality: '75-90',
                        speed: 3
                    }
                }
            }],
            exclude: /node_modules/
        }]
    },
    plugins: [
        new ExtractTextPlugin("style.css"),
        new DashboardPlugin(),
        new CleanWebpackPlugin(['public'], {
          verbose: true,
          dry: false,
          exclude: ['index.html','img','js']
        })
    ],
    resolve: {
        alias: {
            "eventEmitter/EventEmitter": "wolfy87-eventemitter"
        },
        extensions: ['.js', '.jsx']
    },
    devtool: 'source-map',
    // devServer: {
    //     historyApiFallback: true,
    //     contentBase: './'
    // }
};

process.noDeprecation = true;