// Require packages

require('dotenv').config();

const path = require('path'),
    express = require("express"),
    app = express(),
    http = require('http'),
    cors = require('cors'),
    server = http.Server(app);

// Config

const root = path.join(__dirname, '../public');

const port = process.env.PORT || 8080;

// App

app.use(cors());
app.use(express.static(root));


app.use(express.static(path.resolve(__dirname + '../public')));

app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, '..', 'public', 'index.html'));
});


app.listen(port, function() {
    console.log('Client is running on http://localhost:' + port);
});


module.exports = app;
