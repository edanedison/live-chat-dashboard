import './bootstrap.min.js';
import './material.min.js';
import './perfect-scrollbar.jquery.min.js';
import './bootstrap-notify.js';
import './jquery.select-bootstrap.js';
import './jquery.tagsinput.js';
import './material-dashboard.js';