// Imports

const express = require('express');
const path = require('path');

// Authentication route handlers
const Authentication = require('./controllers/authentication');

// Passport middleware module and setup
const passport = require('passport');
const passportStrategies = require('./services/passportStrategies');

const requireAuth = passport.authenticate('jwt', { session: false });
const requireSignin = passport.authenticate('local', { session: false });

// Custom express routing middleware that checks to see if the authenticated user is an admin
const requireAdmin = require('./services/requireAdmin')

// Common Routes
const app = express.Router();

// using requireAuth passport middleware w/ jwt strategy to protect route
app.get('/agent/dashboard', requireAuth, function(req, res) {
  res.send({ message: 'server response:  this GET request has been authorized for a user' });
});

app.get('/profile', requireAuth, Authentication.getProfile);

// using requireAuth passport middleware w/ jwt strategy as well as requireAdmin custom express middleware to protect route
// must be an admin to access admin area
// app.get('/admin_area', requireAuth, requireAdmin, function(req, res, next) {
//   res.send({ message: 'server response:  this GET request has been authorized for an admin' });
// });

// using requireSignin passport middleware to authenticate for protected route using local (email/password) strategy)
// Authentication.signin sends back JWT token to authenticated user
app.post('/signin', requireSignin, Authentication.signin);

// route for signing up user
app.post('/signup', Authentication.signup);

// using requireAuth passport middleware using jwt strategy as well as requireAdmin custom express middleware to protect route
// must be an admin to activate another admin
//app.post('/admin_activation', requireAuth, requireAdmin, Authentication.admin_activation);

module.exports = app;