const express = require('express');
const path = require('path');

// Authentication route handlers
const Authentication = require('../controllers/authentication');

// Passport middleware module and setup
const passport = require('passport');
const passportStrategies = require('../services/passportStrategies');

const requireAuth = passport.authenticate('jwt', { session: false });
const requireSignin = passport.authenticate('local', { session: false });

// Models
const Session = require('../models/session');
const User = require('../models/user');

const api = express.Router();

// IMPORTANT NOTE TO DEVS ... Ensure that all of these routes are protected form public viewing.
// example...
// api.get('/logs/:id', requireAuth,(req, res, next) => {
//     Session.findById(req.params.id).exec((err, document) => {
//         if (err) return next(err);
//         res.json(document);
//     });
// });

// GET method to return a log item

api.get('/logs/:id', (req, res, next) => {
    Session.findById(req.params.id).exec((err, document) => {
        if (err) return next(err);
        res.json(document);
    });
});

// GET method to return all log items .. Can have filters applied

api.get('/logs/', (req, res, next) => {
    Session.find(req.query).lean().exec((err, documents) => {
        if (err) return next(err);
        res.json(documents);
    });
});

// GET method to return sessions for the current day

api.get('/sessions/', (req, res, next) => {

    let start = new Date();
    start.setSeconds(0);
    start.setHours(0);
    start.setMinutes(0);

    let end = new Date(start);
    end.setHours(23);
    end.setMinutes(59);
    end.setSeconds(59);

    req.query["createdAt"] = { $gt: start, $lt: end };

    Session.find(req.query).lean().exec((err, documents) => {
        if (err) return next(err);
        res.json(documents);
    });
});


// GET method to return a user

api.get('/users/:id', (req, res, next) => {
    User.findById(req.params.id).exec((err, document) => {
        if (err) return next(err);
        res.json(document);
    });
});

// GET method to return all users

api.get('/users/', (req, res, next) => {
    User.find(req.query).lean().exec((err, documents) => {
        if (err) return next(err);
        res.json(documents);
    });
});

// Non authenticated routes

// GET method to return the number of agents currently online

api.get('/agents/online/', (req, res, next) => {
    User.find({
        online: true
    }).exec((err, documents) => {
        if (err) return next(err);
        res.json(documents.length);
    });
});

module.exports = api;