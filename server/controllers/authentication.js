const jwt = require('jwt-simple');
const User = require('../models/user');

function tokenForUser(user) {
  const timestamp = new Date().getTime();
  return jwt.encode({ sub: user.id, iat: timestamp, role: user.role }, process.env.SECRET);
}

exports.getProfile = function(req, res, next) {
  // Getting user profile
  res.send({
    profile: req.user.profile,
    email: req.user.email
  });
}

exports.signin = function(req, res, next) {
  // User has been authenticated, send back token
  res.send({
    profile: req.user.profile,
    email: req.user.email,
    token: tokenForUser(req.user)
  });
}

exports.signup = function(req, res, next) {
  const firstName = req.body.firstName;
  const lastName = req.body.lastName;
  const email = req.body.email;
  const password = req.body.password;

  if (!email || !password) {
    return res.status(422).send({ error: 'You must provide email and password'});
  }

  // See if a user with the given email exists
  User.findOne({ email: email }, function(err, existingUser) {
    if (err) { return next(err); }

    // If a user with email does exist, return an error
    if (existingUser) {
      return res.status(422).send({ error: 'Email is in use' });
    }

    // If a user with email does NOT exist, create and save user record
    const user = new User({
      email: email,
      password: password,
      profile: {
        firstName: firstName,
        lastName: lastName
      },
      role: 'agent'
    });

    user.save(function(err) {
      if (err) { return next(err); }

      // Repond to request indicating the user was created
      res.json({
        profile: user.profile,
        token: tokenForUser(user)
      });
    });
  });
}


// exports.admin_activation = function(req, res, next) {
//   const email = req.body.email;
//   const password = req.body.password;

//   if (!email || !password) {
//     return res.status(422).send({ error: 'You must provide email and password'});
//   }

//   // See if an user with the given email exists
//   User.findOne({ email: email }, function(err, existingUser) {
//     if (err) { return next(err); }

//     // If an user with email does exist, return an error
//     if (existingUser) {
//       return res.status(422).send({ error: 'Email is in use' });
//     }

//     // If a user with email does NOT exist, create and save record for admin
//     const user = new User({
//       email: email,
//       password: password,
//       role: 'admin'
//     });

//     user.save(function(err) {
//       if (err) { return next(err); }

//       // Repond to request indicating the admin was created
//       // res.send({});
//     });
//   });
// }
