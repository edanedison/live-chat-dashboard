const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MessageSchema = new Schema({
    createdAt: Date,
    agent: {
    	type: Boolean,
    	default: false
    },
    message: String
});

module.exports = mongoose.model('Message', MessageSchema);