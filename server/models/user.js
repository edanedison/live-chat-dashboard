const bcrypt = require('bcrypt-nodejs');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    email: {
        type: String,
        unique: true,
        lowercase: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    profile: {
        firstName: {
            type: String
        },
        lastName: {
            type: String
        },
        photo: {
            type: String,
            default: '/img/default-avatar.png'
        }
    },
    role: {
        type: String,
        enum: ['agent', 'manager', 'admin'],
        default: 'agent'
    },
    online: {
        type: Boolean,
        default: '/img/default-avatar.png'
    }
});

// Before saving a model, encrypt the password
UserSchema.pre('save', function(next) {
    // get access to the user model
    const user = this,
        SALT_FACTOR = 10;

    // generate a salt then run callback
    bcrypt.genSalt(SALT_FACTOR, function(err, salt) {
        if (err) {
            return next(err);
        }

        // hash (encrypt) our password using the salt
        bcrypt.hash(user.password, salt, null, function(err, hash) {
            if (err) {
                return next(err);
            }

            // overwrite plain text password with encrypted password
            user.password = hash;
            next();
        });
    });
});

//to be used in passport.js

UserSchema.methods.comparePassword = function(candidatePassword, callback) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) {
            return callback(err);
        }
        callback(null, isMatch);
    });
}

module.exports = mongoose.model('User', UserSchema);