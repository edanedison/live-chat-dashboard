const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SessionSchema = new Schema({
    sessionId: String,
    createdAt: Date,
    accepted: {
    	type: Boolean,
    	default: false
    },
    acceptedAt: Date,
    completed: {
    	type: Boolean,
    	default: false
    },
    completedAt: Date,
    completedBy: String,
    dropped: {
        type: Boolean,
        default: false
    },
    droppedAt: Date,
    droppedBy: String,
    terminated: {
        type: Boolean,
        default: false
    },
    terminatedAt: Date,
    terminatedBy: String,
    timedOut: {
        type: Boolean,
        default: false
    },
    customerData: Object,
    agentData: Object,
    transcript: Array()
});

module.exports = mongoose.model('Session', SessionSchema);