const Session = require('../models/session');
const User = require('../models/user');
const Message = require('../models/message');

let session, log = [];

let self = module.exports = {

    updateAllClients: function(io) {
            io.emit('UPDATE_AVAILABLE_AGENTS');
            io.emit('UPDATE_QUEUE_POSITIONS');
            io.emit('UPDATE_CUSTOMER_QUEUE');
            io.emit('UPDATE_CURRENT_STATS');
    },


    listenForAgentStatus: function(socket, io) {

       socket.on('AGENT_STATUS_CHANGE', (data) => {

            User.update({'email': data.username}, { // TODO: do this via api 'patch' method
                online: data.status }, function(err, result) {
                if (!err) {
                    // console.log(result)
                } else {
                    console.log(err)
                }
            });

            self.updateAllClients(io);

        });
    },

    listenForCustomers: function(socket, io) {

       socket.on('CUSTOMER_WAITING', (data) => {

            let sessionId = socket.id,
                mongoose = require('mongoose'),
                ObjectId = mongoose.Types.ObjectId;

            session = new Session({
                createdAt: Date.now(),
                sessionId,
                customerData: data,
                transcript: []
            });

            session.save(function(err, msg) {
                console.log('Session log created, waiting for an agent...', msg);
            });

            io.emit('NEW_CUSTOMER_ALERT', {
              customerName: data.name
            });

            self.updateAllClients(io);

        });
    },

    listenForInitiation: function(socket, io) {

        // At this point, the agent has clicked the 'start session' button and we will attempt to connect him to the waiting customer

        socket.on('INITIATE_CHAT', (data) => {

            if (data.sessionId) {

                console.log('Agent', data.agentEmail, 'has initiated session with', data.customerName, 'under session id', data.sessionId)

                const agentData = {
                    'name': data.agentFirstName,
                    'email': data.agentEmail
                };

                Session.update({sessionId: data.sessionId}, {
                    accepted: true, acceptedAt: Date.now(), agentData  }, function(err, result) {
                    if (!err) {
                        // console.log(result)
                    } else {
                        console.log(err)
                    }
                });

                socket.broadcast.emit('OPEN_CHAT_ROOM', {
                    sessionId: data.sessionId,
                    customerName: data.customerName,
                    agentName: data.agentFirstName // Only passing the agent's first name to the customer for security purposes
                });

            }

            self.updateAllClients(io);

        });
    },

    listenForTermination: function(socket, io) {

        // This is called when either the Agent or Customer ends the chat via the 'end chat' button

        socket.on('CHAT_TERMINATED', (data) => {

            console.log('Session', data.sessionId, 'has been terminated by', data.user  )

            Session.findOne({sessionId: data.sessionId}, function (err, session) {
                session.dropped = false;
                session.completed = true;
                session.terminated = true;
                session.terminatedAt = Date.now();
                session.timedOut = data.timeout || false;
                session.completedAt = Date.now();
                session.completedBy = data.user;
                session.save(function (err) {
                    if(err) {
                        console.error(err);
                    }
                });
            });

            io.to(data.sessionId).emit('DROP_PARTICIPANTS', {
                isAgent: data.isAgent
            });

            self.updateAllClients(io);

        });
    },


    listenForAbandonment: function(socket, io) {

        socket.on('disconnecting', () => {
            // Find the name of the room that the disconnecting socket belongs to.
            // There will be a room only if a chat has been initiated....
            Object.keys(socket.rooms).forEach(function(room, idx) {
                if(idx!=0){
                    // console.log('Chat has been abandoned early...')
                    // console.log(JSON.stringify(socket.adapter.rooms, null, 4));
                    // console.log(idx,"-->",room)
                    Session.update({sessionId: room, "accepted": {$eq: true}}, {
                        dropped: false,
                        terminated: true,
                        terminatedAt: Date.now(),
                        completed: true,
                        completedAt: Date.now(),
                    }, function(err, result) {
                        if (!err) {
                            // console.log(result)
                        } else {
                            console.log(err)
                        }
                    });

                    io.in(room).emit('DROP_PARTICIPANTS', {
                        user: socket.id
                    });
                }
             });

            // Otherwise the customer will still be waiting in the queue and this is classed as a dropout
            Session.update({sessionId: socket.id, "accepted": {$ne: true}}, {
                dropped: true,
                droppedAt: Date.now(),
                droppedBy: 'Customer'
            }, function(err, result) {
                if (!err) {
                    // console.log(result)
                } else {
                    console.log(err)
                }
            });

            self.updateAllClients(io);

        });
    },


    listenForTyping: function(socket, io) {

        socket.on('TYPING_STARTED', (data) => {
            let user = data.agent ? 'Agent' : 'Customer';
            console.log(user + " started typing");
            socket.to(data.sessionId).emit('BROADCAST_TYPING_STARTED', {
                id: data.sessionId
            });

        });

        socket.on('TYPING_STOPPED', (data) => {
            let user = data.agent ? 'Agent' : 'Customer';
            console.log(user + " stopped typing");
             socket.to(data.sessionId).emit('BROADCAST_TYPING_STOPPED', {
                id: data.sessionId
            });
        });
    },


    listenForMessages: function(socket, io) {

        socket.on('MESSAGE_SENT', (data) => {

            // Save incoming message to the database

            let message = new Message({
                    message: data.message,
                    createdAt: Date.now(),
                    agent: data.agent | false
            });

            Session.update({sessionId: data.sessionId}, {$push:{transcript:message}}, function(err, result) {
                if (err) {
                  console.log(err);
                } else {
                    console.log("session log updated")
                }
            });

            // Broadcast the message to the other user

            io.to(data.sessionId).emit('BROADCAST_MESSAGE', {
                message: data.message,
                createdAt: Date.now(),
                agent: data.agent | false,
                // sessionId: data.sessionId
            });

        });
    },


}

