require('dotenv').config()

const path = require('path');
const express = require('express');
const http = require('http');
const bodyParser = require('body-parser'); //middleware which parses HTTP request bodies and makes them available in req.body
const morgan = require('morgan'); //HTTP request logger middleware
const router = require('./router');
const mongoose = require('mongoose');
const cors = require('cors');
const request = require('request');
const socketio = require('socket.io');
const app = express();
const server = http.Server(app);
const io = socketio(server);

const port = process.env.PORT || 3000;

const routes = require('./router');
const api = require('./api');
const utils = require('./utils');

// Mongoose setup

mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGODB_URI || 'mongodb://localhost/cs-dashboard', {
    useMongoClient: true,
    promiseLibrary: require('bluebird')
});

mongoose.set('debug', false)

// App setup

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cors());
app.use(routes);
app.use('/api', api);

app.set('json spaces', 2); // Pretty printing

// Start server

server.listen(port, () => {
    console.log('Listening on port: ' + port);
});

// Socket actions

let agentConnected = false,
    customerConnected = false,
    isAgent = false,
    isCustomer = false;

var rooms = [];

io.use(function(socket, next) {

    // This bit is a sort of 'pre socket connection' conditional code. Depending on the http referrer,
    // it is possible to set certain variables etc before executing 'next()' which will proceed to connect the actual socket.

console.log(socket.request.headers.referer)

    const host = socket.request.headers.origin;
    const referer = socket.request.headers.referer

    switch (true) {
        case (referer === host + '/agent/queue'):
            agentConnected = false;
            isAgent = true;
            isCustomer = false;
            next();
            break;
        case (referer === host + '/agent/dashboard'):
            isAgent = true;
            isCustomer = false;
            next();
            break;
        case (referer === host + '/agent/chat'):
            agentConnected = true;
            isAgent = true;
            isCustomer = false;
            next();
            break;
        case (referer === host + '/customer/queue'):
            customerConnected = true;
            isCustomer = true;
            next();
            break
        default:
            isCustomer = true;
            next();
    }

});

io.on('connection', (socket) => {

    utils.listenForCustomers(socket, io);
    utils.listenForInitiation(socket, io);
    utils.listenForTermination(socket, io);
    utils.listenForMessages(socket, io);
    utils.listenForTyping(socket, io);

    if (agentConnected) {
        socket.emit('AGENT_CONNECTED');
    }

    if (customerConnected) {
        io.emit('CUSTOMER_CONNECTED', {
            sessionId: socket.id
        });
    }

    if (isAgent) {
        utils.listenForAgentStatus(socket, io);
    }

    if (isCustomer) {
        utils.listenForAbandonment(socket, io);
    }

    socket.on('SECURE_CHAT_ROOM', function(id) {
        socket.join(id);
    });

    socket.on('BROADCAST_AGENT_DETAILS', function(data) {
        console.log(data)
        socket.broadcast.emit('RECEIVE_AGENT_DETAILS', data);
    });




});